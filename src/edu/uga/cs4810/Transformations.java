package edu.uga.cs4810;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;
import java.util.Scanner;

public class Transformations {
	
	public static BufferedImage image;
	public static int num;
	private int vWidth, vHeight;
	private boolean notVisible = false;
	
	public double[][] BasicTranslate(int Tx, int Ty) {
		double[][] matrix = {{1,  0,  0},
							 {0,  1,  0}, 
							 {Tx, Ty, 1}};
		return matrix;
	}
	
	public double[][] BasicScale(int Sx, int Sy) {
		if(Sx == 0) Sx=1;
		if(Sy == 0) Sy=1;
		double[][] matrix = {{Sx, 0,  0}, 
							 {0,  Sy, 0}, 
							 {0,  0,  1}}; 
		return matrix;
	}
	
	public double[][] BasicRotate(double angle) {
		double theta = Math.toRadians(angle);
		double cos = Math.cos(theta);
		double sin =  Math.sin(theta);
		double[][] matrix = {{cos, -sin,  0}, 
							 {sin,  cos,  0}, 
						   	 { 0,    0,   1}};
		return matrix;
	}
	
	public double[][] Concatenate(double[][] matrix1, double[][] matrix2) {
		double[][] product = new double[3][3];

			for(int i = 0; i < 3; i++) {
				for(int j = 0; j < 3; j++) {
					for(int k = 0; k < 3; k++) {
						product[i][j] += matrix1[i][k]*matrix2[k][j]; 
					}
						
				}
			}
			return product;
		}
	
	public double[][] ApplyTransformation(double[][] matrix, double[][] datalines, int num) {
		
		for(int i = 0; i<num; i++) {
			datalines[i][0] = datalines[i][0]*matrix[0][0] + datalines[i][1]*matrix[1][0] + matrix[2][0];
			datalines[i][1] = datalines[i][0]*matrix[0][1] + datalines[i][1]*matrix[1][1] + matrix[2][1];
			datalines[i][2] = datalines[i][2]*matrix[0][0] + datalines[i][3]*matrix[1][0] + matrix[2][0];
			datalines[i][3] = datalines[i][2]*matrix[0][1] + datalines[i][3]*matrix[1][1] + matrix[2][1];
		}
		return datalines;
	}
	
	public int bitCode(int x, int y) {
		int code = 0;   
		if(x<0) code |= 1;
		else if(x>getvWidth()) code |= 2;
		if(y<0) code |= 8;
		else if(y>getvHeight()) code |= 4;
		return code;
	}

	public int[] clipping(int x1, int y1, int x2, int y2) {
	int[] newValues = new int[4];
		boolean one = false;
		boolean two = false;
		int x = 0;
		int y= 0;
		int code1 = 0;
		int code2 = 0;
		notVisible = false;
		boolean visible = false;
		boolean done = false;
		while(done == false) {
			one = false;
			two = false;
			code1 = bitCode(x1, y1);
			code2 = bitCode(x2, y2);
			if((code1 | code2) == 0) {
				visible = true;
				break;
			}
			else if((code1 & code2) != 0) {
				visible = false;
				notVisible = true;
				break;
			}


			else {//
				int codeout;
				if(code1 > 0) {
					codeout=code1;
					one=true;
				}
				else {
					codeout=code2;
					two=true;
				}

				///Check this bug
				if(one) {
					if((codeout & 1) == 1) x1=0;
					else if((codeout & 2) == 2) x1=getvWidth();


					if((codeout & 8) == 8) y1=0;
					else if((codeout & 4) == 4) y1=getvHeight();

				}
				
				else {
					if((codeout & 1) == 1) x2=0;
					else if((codeout & 2) == 2) x2=getvWidth();


					if((codeout & 8) == 8) y2=0;
					else if((codeout & 4) == 4) y2=getvHeight();

				}

			}//

		}//end while
		newValues[0] = x1;
		newValues[1] = y1;
		newValues[2] = x2;
		newValues[3] = y2;
		
		System.out.println("x1:  " + newValues[0] + "   y1:  " + newValues[1] + "   x2:  " + newValues[2] + "   y2:  " + newValues[3]);

		return newValues;
	}

	public BufferedImage ViewportSpec(int width, int height) {
		Color black = new Color(0,0,0);
		int rgb = black.getRGB();
		image = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		for(int i=0; i<width; i++) {
			for(int j=0; j<height; j++) {
				image.setRGB(i, j, rgb);			
			}
		}		
		//Graphics g = image.getGraphics();
		setWidth(width);
		setHeight(height);
		System.out.println("Viewport Method|    Width: " + getvWidth() + "    Height: " + getvHeight());
		return image;
	}


	public BufferedImage Displaypixel(double[][] datalines) {
		
		int num = getNumber();
		Color green = new Color(128, 255, 0); 
		int rgb = green.getRGB();
		
		int dx, dy, E, ystep, x2, x1, y2, y1;
		
		System.out.println("NUMBER OF LINES:   " + num );
		for(int i=0; i<num; i++) {
			
			
			x1 = (int) datalines[i][0];
			y1 = (int) datalines[i][1];
			x2 = (int) datalines[i][2];
			y2 = (int) datalines[i][3];
			
			int[] newValues = clipping(x1, y1, x2, y2);
			
			if(notVisible == true) continue;
			
			x1 = newValues[0];
			y1 = newValues[1];
			x2 = newValues[2];
			y2 = newValues[3];

			boolean swap = Math.abs(y2-y1) > Math.abs(x2-x1);
			if(swap) { //if slope is greater than or equal to 1
				int temp = x1;
				x1 = y1;
				y1 = temp;
				temp = x2;
				x2 = y2;
				y2 = temp;
			}
			if(x1>x2) { //switches starting point and end point
				//swap x1 and x2, swap y1 and y2
				int temp = x1;
				x1 = x2;
				x2 = temp;
				temp = y1;
				y1 = y2;
				y2 = temp;
			}
			dx = x2 - x1;
			dy = Math.abs(y2 - y1);
			E = dx/2;
			int y = y1;

			if(y1<y2) { //if positive slope
				ystep = 1;
			}
			else { //if negative slope
				ystep = -1;
			}

			for(int x = x1; x<x2;x++) {

				if(swap) {
					image.setRGB(y,x,rgb);
				}
				else {
					image.setRGB(x,y,rgb);
				}
				E = E-dy;
				if(E<0) {
					y = y+ystep;
					E = E+dx;
				}

			}
		}
		return image;
	}
	
	public double[][] Inputlines(String data) throws FileNotFoundException {
		
		int i=0;
		int j=0;

		File file = new File(data);
		int number = 0;
		Scanner scanner = new Scanner(file);
		while(scanner.hasNextLine())  {
			number++; 
			scanner.nextLine();
		}
		//System.out.println(number);                                                    
		
		double[][] datalines = new double[number][4];
		//System.out.println(datalines.length + "    " + datalines[0].length);
		Scanner scanner2 = new Scanner(file);
		while(scanner2.hasNextInt()) {
				datalines[i][j] = scanner2.nextInt();
				//System.out.println("I: " + i + "      J: " + j + "     Data: " + datalines[i][j]);
				if(i==(number)) i=0;
				if(j==3) {j=-1; i++;};
				j++;
		}
		
		System.out.println(Arrays.deepToString(datalines));
		setNumber(number);//
		return datalines;
	}
	
	public void getLineNumber(double[][] datalines) {
		
	}
	
	public void setNumber(int number) {
		this.num = number;
	}
	
	public int getNumber() {
		return num;
	}

	public void setHeight(int height) {
		this.vHeight = height;
	}
	
	public void setWidth(int width) {
		this.vWidth = width;
	}
	
	public int getvWidth() {
		return vWidth;
	}
	
	public int getvHeight() {
		return vHeight;
	}
	
	
}



