package edu.uga.cs4810;

import java.awt.BorderLayout;	
import java.awt.Button;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Arrays;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SpringLayout;

import net.miginfocom.swing.MigLayout;


public class UserInterface extends JPanel{
	
	private static int vWidth=0;
	private static int vHeight=0;
	private static int xCoordinate;
	private static int yCoordinate;
	private static JButton drawLines;
	protected static JButton open;
	private static JButton createViewport;
	private static JLabel viewportSize, widthLabel, heightLabel, viewportLocation, x, y;
	private static JTextField viewportWidth, viewportHeight, xLocation, yLocation;
	private static double[][] datalines;
	private static boolean clicked = false;
	private static boolean clicked2 = false;
	private static int num;
	
	
	public void paint(Graphics g) {
		if(clicked == true) {
			super.paint(g);
			Transformations image = new Transformations();
			double[][] data;
			BufferedImage viewport = image.ViewportSpec(vWidth, vHeight);
			if(clicked2==true) {
			data = datalines; 
			viewport = image.Displaypixel(data);
			}
			g.drawImage(viewport, xCoordinate,yCoordinate, this);
		}
	}

	public static void createAndShowGUI() {

		final JFrame frame = new JFrame("2D Basic Transformations");
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		final JFileChooser fc = new JFileChooser();
		
		frame.getContentPane().setLayout(new BorderLayout());
		
		JPanel inputPanel = new JPanel();
		inputPanel.setPreferredSize(new Dimension(300, 786));
		inputPanel.setBackground(Color.GRAY);
		frame.getContentPane().add(inputPanel, BorderLayout.LINE_START);
		
		
		MigLayout mig = new MigLayout(
				"",
				"[65:65:65][65:65:65][65:65:65][65:65:65]",
				"[]20[]5[]20[]5[]50[]50[]40[]5[]15[]5[]15[]5[]40[]"
				);
		
		inputPanel.setLayout(mig);
		
		open = new JButton("Open File");
		JLabel viewportSize = new JLabel("ViewPort Dimesions");
		JLabel widthLabel = new JLabel("Width:");
		JLabel heightLabel = new JLabel("Height:");
		final JTextField viewportWidth = new JTextField(5);
		final JTextField viewportHeight = new JTextField(5);
		JLabel viewportLocation = new JLabel("ViewPort Location:");
		JLabel x = new JLabel("x:");
		JLabel y = new JLabel("y:");
		final JTextField xLocation = new JTextField(5);
		final JTextField yLocation = new JTextField(5);
		createViewport = new JButton("Create Viewport");
		drawLines = new JButton("Draw Lines");
		JLabel translate = new JLabel("Translate:");
		JLabel scale = new JLabel("Scale:");
		JLabel rotate = new JLabel("Rotate:");
		JLabel translateX = new JLabel("x:");
		JLabel translateY = new JLabel("y:");
		JLabel scaleX = new JLabel("x:");
		JLabel scaleY = new JLabel("y:");
		final JTextField translateXField = new JTextField(30);
		final JTextField translateYField = new JTextField(30);
		final JTextField scaleXField = new JTextField(40);
		final JTextField scaleYField = new JTextField(40);
		final JTextField rotateField = new JTextField(50); 
		JButton applyTransformations = new JButton("Apply Transformations");

		inputPanel.add(open, "cell 1 0, width 100:100:100, gapleft 15");
		
		
		inputPanel.add(viewportSize, "cell 1 1");
		
		inputPanel.add(widthLabel, "cell 0 2");
		inputPanel.add(viewportWidth, "cell 1 2");
		inputPanel.add(heightLabel, "gapleft 10");
		inputPanel.add(viewportHeight, "cell 3 2");
		
		
		inputPanel.add(viewportLocation, "cell 1 3, gapleft 7");
		
		
		inputPanel.add(x, "cell 0 4");
		inputPanel.add(xLocation, "cell 1 4");
		inputPanel.add(y, "gapleft 10");
		inputPanel.add(yLocation, "cell 3 4");
		
		inputPanel.add(createViewport, "cell 0 5, width 200:200:200, gapleft 35");
		
		inputPanel.add(drawLines, "cell 0 6, width 150:150:150, gapleft 60");
		
		
		inputPanel.add(translate, "cell 0 7");
		inputPanel.add(translateX, "cell 0 8, gapleft 15");
		inputPanel.add(translateXField, "cell 1 8");
		inputPanel.add(translateY, "cell 2 8, gapleft 15");
		inputPanel.add(translateYField, "cell 3 8");
		
		
		inputPanel.add(scale, "cell 0 9");
		inputPanel.add(scaleX, "cell 0 10, gapleft 15");
		inputPanel.add(scaleXField, "cell 1 10");
		inputPanel.add(scaleY, "cell 2 10, gapleft 15");
		inputPanel.add(scaleYField, "cell 3 10");
		
		
		inputPanel.add(rotate, "cell 0 11");
		inputPanel.add(rotateField, "cell 0 12, gapleft 70, span 2");
		inputPanel.add(applyTransformations, "cell 0 13, gapleft 40");
		
		
		applyTransformations.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				clicked = true;
				double[][] translate, scale, rotate, result;
				Transformations transform = new Transformations();
				translate = transform.BasicTranslate(Integer.parseInt(translateXField.getText()), Integer.parseInt(translateYField.getText()));
				System.out.println("Translation Matrix:  " + Arrays.deepToString(translate));
				scale = transform.BasicScale(Integer.parseInt(scaleXField.getText()), Integer.parseInt(scaleYField.getText()));
				System.out.println("Scale Matrix:  " + Arrays.deepToString(scale));
				rotate = transform.BasicRotate(Integer.parseInt(rotateField.getText()));
				System.out.println("Rotate Matrix:  " + Arrays.deepToString(rotate));
				
				
				result = transform.Concatenate(translate, scale);
				System.out.println("Translate x Scale:  " + Arrays.deepToString(result));
				result = transform.Concatenate(result, rotate);
				System.out.println("Result x Rotate:  " + Arrays.deepToString(result));
				
				System.out.println("Original Datalines:  " + Arrays.deepToString(datalines));
				
				
				datalines = transform.ApplyTransformation(result, datalines, datalines.length);
				System.out.println("New Datalines:  " + Arrays.deepToString(datalines));
				frame.repaint();
			
				
			}
		});
		
		createViewport.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				vWidth = Integer.parseInt(viewportWidth.getText());
				vHeight = Integer.parseInt(viewportHeight.getText());
				xCoordinate = Integer.parseInt(xLocation.getText());
				yCoordinate = Integer.parseInt(yLocation.getText());
				clicked = true;
				frame.repaint();
			}
		});

		open.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent e)
			{
				int returnVal = fc.showOpenDialog(null);
				if (returnVal == JFileChooser.APPROVE_OPTION) {
					String data = fc.getSelectedFile().getAbsolutePath();
					Transformations newFile = new Transformations();
					try {
						datalines = newFile.Inputlines(data);
						//num = newFile.getLineNumber();
					} catch (FileNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();

					}
				}
			}
		});
		
		
		drawLines.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				clicked2=true;
				
				frame.repaint();
			}
		});
		
		
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		frame.setBounds(0, 0 , screenSize.width, screenSize.height);
		frame.getContentPane().add(new UserInterface());
		frame.setVisible(true);
	}


	public static void main(String[] args) {
		javax.swing.SwingUtilities.invokeLater(new Runnable() {
			public void run() {
				createAndShowGUI();
			} // run
		});

	} // main
	
	//Getters and Setters
	
	
	public int getViewportWidth() {
		return vWidth;
	}
	
	public int getViewportHeight() {
		return vHeight;
	}
	
	public int getXCoordinate(){
		return xCoordinate;
	}
	public int getYCoordinate(){
		return yCoordinate;
	}
	
	
	public void setWeight(String height) {
		this.vWidth = Integer.parseInt(height);
	}
	
	public void setHeight(String height) {
		this.vHeight = Integer.parseInt(height);
	}
	
	public void setXCoordinate(String xCoordinate){
		this.xCoordinate = Integer.parseInt(xCoordinate);
	}
	public void setYCoordinate(String yCoordinate){
		this.yCoordinate = Integer.parseInt(yCoordinate);
	}
	
	
		
}


